Please keep in mind that this is my first node.js project ever : ) 

Because it was fun to finally put the tutorials into practice, I am also going to do this with Nest.js. 
It would actually be "production-worthy" because It's taking care of the node/webpack boilerplate work that I'm no expert of. :]

- I used DotEnv for GOOGLE_MAPS_API_KEY var.
- I wrote 3 simple tests to demonstrate basic familiarity with TDD
- For performance, I thought about caching Google Geocode results, but that's against the APIs terms of use. 


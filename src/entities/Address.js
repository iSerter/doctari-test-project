export default class Address {
    constructor(streetLine, postalCode, locality, state, country) {
        this.streetLine = streetLine;
        this.postalCode = postalCode;
        this.locality = locality;
        this.state = state;
        this.country = country;

    }

    get formattedAddress() {
        return [this.streetLine, `${this.postalCode} ${this.locality}`.trim(), this.state, this.country]
            .filter(v => v) // removes empty ones
            .join(', ')
            .trim();
    }
}

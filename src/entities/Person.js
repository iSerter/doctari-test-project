export class Person {
    constructor(name, address) {
        this.name = name;
        this.address = address;
    }

    get firstName() {
        return this.name.split(' ').shift();
    }

    get lastName() {
        return this.name.split(' ').pop();
    }

}
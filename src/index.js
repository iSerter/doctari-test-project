import readFile from './utils/readFile';
import parseTestData from "./utils/parseTestData";
import createDomElement from "./utils/createDomElement";
import {GoogleGeocodeApi} from "./utils/GoogleGeocodeApi";
import Address from "./entities/Address";

const geoApi = new GoogleGeocodeApi(process.env.GOOGLE_MAPS_API_KEY);

const app = document.getElementById('root');

function createEntryElement(entry) {
    const el = createDomElement('div', {className: "entryContainer"});
    const personName = createDomElement('h3', {innerText: entry.person.name});
    el.appendChild(personName);

    const initialAddress = createDomElement(
        'p',
        {className: "initialAddress", innerText: entry.formattedAddress}
    );

    el.appendChild(initialAddress);

    el.appendChild(createDomElement('pre', {className: "apiResult", innerText: '...click me...'}));

    return el;
}

readFile('../data/testdaten.txt').then((content) => {
    const peopleWithAddresses = parseTestData(content);
    peopleWithAddresses.forEach(entry => {
        const entryEl = createEntryElement(entry);
        app.appendChild(entryEl);
        entryEl.querySelector('.apiResult')
            .addEventListener('click', e => {
                geoApi.query(entry.formattedAddress).then(res => {
                    const resultEl = e.target;
                    if (resultEl && res.results[0]) {
                        const components = res.results[0].address_components || [];
                        const streetName = geoApi.extractAddressComponent(components, 'route');
                        const streetNo = geoApi.extractAddressComponent(components, 'street_number');
                        const address = new Address(
                            `${streetName} ${streetNo}`.trim(),
                            geoApi.extractAddressComponent(components, 'postal_code'),
                            geoApi.extractAddressComponent(components, 'locality'),
                            geoApi.extractAddressComponent(components, 'administrative_area_level_2')
                            || geoApi.extractAddressComponent(components, 'administrative_area_level_1'),
                            geoApi.extractAddressComponent(components, 'country'),
                        );
                        resultEl.innerText = JSON.stringify(address);
                    } else if (resultEl) {
                        resultEl.innerText = 'Invalid!';
                    }
                });
            });
    });
});


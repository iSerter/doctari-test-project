export class GoogleGeocodeApi {
    constructor(apiKey) {
        this.apiKey = apiKey;
    }

    query(address) {
        return fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${this.apiKey}`)
            .then(response => response.json());
    }

    extractAddressComponent(components, type) {
        return components
            .filter((component) => component.types.indexOf(type) === 0)
            .map((item) => item.long_name)
            .pop() || null;
    }
}
export default function createDomElement(el, options) {
    let element = document.createElement(el);
    Object.keys(options).forEach(function (k) {
        element[k] = options[k];
    });
    return element;
}
import {Person} from "../entities/Person";

export default function parseTestData(text) {
    const entries = [];
    const textEntries = text.trim().split(/\n\s*\n/);
    textEntries.forEach(text => {
        const lines = text.split('\n');
        entries.push({
            person: new Person(lines.shift()),
            formattedAddress: lines.join(' '),
        });
    });
    return entries;
}
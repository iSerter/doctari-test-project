import Address from "../../src/entities/Address";

test("Address entity provides formattedAddress", () => {
    const addr = new Address('Adnan Menderes Blv 19', '09400', 'Kusadasi', 'Aydin');
    expect(addr.formattedAddress).toBe("Adnan Menderes Blv 19, 09400 Kusadasi, Aydin");
});
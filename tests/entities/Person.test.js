import { Person } from "../../src/entities/Person";

test('Person entity returns correct first name', () => {
    const person = new Person("ilyas serter");
    expect(person.firstName).toBe('ilyas');
});

test('Person entity returns correct last name', () => {
    const person = new Person("ilyas serter");
    expect(person.lastName).toBe('serter');
});
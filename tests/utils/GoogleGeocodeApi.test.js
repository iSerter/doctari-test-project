import {GoogleGeocodeApi} from "../../src/utils/GoogleGeocodeApi";

const sampleAddressComponents = [
    {
        "long_name": "Fischerinsel",
        "short_name": "Fischerinsel",
        "types": [
            "route"
        ]
    },
    {
        "long_name": "Mitte",
        "short_name": "Mitte",
        "types": [
            "political",
            "sublocality",
            "sublocality_level_1"
        ]
    },
    {
        "long_name": "Berlin",
        "short_name": "Berlin",
        "types": [
            "locality",
            "political"
        ]
    },
    {
        "long_name": "Kreisfreie Stadt Berlin",
        "short_name": "Kreisfreie Stadt Berlin",
        "types": [
            "administrative_area_level_3",
            "political"
        ]
    },
    {
        "long_name": "Berlin",
        "short_name": "BE",
        "types": [
            "administrative_area_level_1",
            "political"
        ]
    },
    {
        "long_name": "Germany",
        "short_name": "DE",
        "types": [
            "country",
            "political"
        ]
    }
];

test('extractAddressComponent extracts country', () => {
    const geoAPI = new GoogleGeocodeApi();
    expect(geoAPI.extractAddressComponent(sampleAddressComponents, 'country')).toBe('Germany')
});

test('extractAddressComponent extracts locality', () => {
    const geoAPI = new GoogleGeocodeApi();
    expect(geoAPI.extractAddressComponent(sampleAddressComponents, 'locality')).toBe('Berlin')
})
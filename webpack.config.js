const Dotenv = require('dotenv-webpack');

module.exports = {
    watch: true,
    plugins: [
        new Dotenv()
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    }
};
